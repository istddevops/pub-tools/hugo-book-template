# HUGO Book Template

HUGO SSG Book Template voor het creeren van een nieuw documentatie project in GitLab.

## Lokaal ontwikkelen op PC/Mac

Start lokale ontwikkeltool zoals [Visual Studio Code](https://code.visualstudio.com/) en open een nieuwe ontwikkelomgeving (```File > New Window``` in Visual Studio Code). Open vervolgens een *Terminal* (```Terminal > New Terminal``` in Visual Studio Code) en voer daarin onderstaande commando's[^1] achtereenvolgens uit:

```shell
git clone https://<GitLab Username>:<GitLab Personal Access Token>@gitlab.com/istddev/pub-tools/hugo-book-template.git
git submodule update --init --recursive
```

Daarna heb je een lokale kopie die via ```GIT``` is gekoppeld aan de GitLab repository. Start de *HUGO ontwikkel server* als volgt:

```shell
hugo server
```

Vervolgens kun je onder de ```content``` directory de huidige gepubliceerde inhoud vinden. In principe kan je de **markdown**-content aanvullen en wijzigen onder de ```content/docs``` directorie. Kijk naar de voorbeelden en experimenteer. Elke pagina (in **markdown**) begint met een zogenaamde **Frontmatter**. Zie onderstaande voorbeeld:

```yaml
---
title: "Diagrammen (KROKI API)"
weight: 1
---
```

Het veld ```weight``` geeft daarin de rangorde waarin de te publiceren pagina wordt getoond. Daarboven bepaald de directorie-structuur het niveau. Elke directorie kan een ```_index.md``` bestand kan bevatten waarmee de volgorde tussen directories op hetzelfde niveau kan worden bepaald via het ```weight``` veld.


Zie [HUGO Content Management](https://gohugo.io/content-management/) en [Hugo Book Theme](https://themes.gohugo.io/hugo-book/) voor de uitleg over de werking en geavanceerde opties.

## Online ontwikkelen via GitPod

Open vanuit deze GitLab Project(repository) [HUGO Book Template](https://gitlab.com/istddev/pub-tools/hugo-book-template) door **[GitPod]** knop rechts naast de **[Find file]** knop te selecteren.

Wacht tot Online omgeving is geladen. Daarna zal de *HUGO ontwikkel server* automatisch opstarten en een Preview venster met de site verschijnen. Zie ook lokaal ontwikkelen over inhoud bekijken en aanpassen.

[^1]: Er wordt hierbij vanuit gegaan dat: de ```<GitLab Username>``` toegangsrechten heeft om te kunnen ontwikkelen; en de ```Personal Access Token``` lees- en schrijfrechten op repositories (scopes: read_repository, write_repository) geeft